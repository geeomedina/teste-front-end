# Teste para vaga de desenvolvedor Front-End
O teste consiste em desenvolver uma página estática e responsiva.

## Instruções:

* Tenha certeza que possui instalados em sua máquina: <b>Node, NPM, Grunt e Ruby/Sass</b>.

* Mantenha os diretórios dentro de /build;

* Aplique os seguintes comandos, localmente, na pasta do projeto: 
- `npm install grunt --save-dev`
- `npm install grunt-contrib-sass --save-dev`
- `npm install grunt-font-awesome-list --save-dev`
- `npm install grunt-html-build --save-dev`

* Dê o comando `grunt` para compilar e realizar o build;

* Abra o arquivo `index.html` para visualização.
