//Header
window.onscroll = function() {
  myFunction()
};
var header = document.getElementById("header");
var fix = header.offsetTop;
function myFunction() {
  if(window.pageYOffset > fix) {
    header.classList.add("fix-header");
  }
  else {
    header.classList.remove("fix-header");
  }
}

//Fade
var sldIdx = 1;
showImages(sldIdx);

function slideNow(n) {
  showImages(sldIdx = n);
}

function showImages(n) {
  var i;
  var slides = document.getElementsByClassName("slides");
  var dots = document.getElementsByClassName("dot");
  if(n > slides.length) {
    sldIdx = 1
  }
  if(n < 1) {
    sldIdx = slides.length
  }
  for(i = 0; i < slides.length; i++) {
      slides[i].style.display = "none";
  }
  for(i = 0; i < dots.length; i++) {
      dots[i].className = dots[i].className.replace(" active", "");
  }
  slides[sldIdx-1].style.display = "block";
  dots[sldIdx-1].className += " active";
}