module.exports = function(grunt) {
  'use strict';

  var gruntConfig = {
    pkg: grunt.file.readJSON('package.json'),
    BASE_PATH: '',
    DEVELOPMENT_PATH: 'src/',
    PRODUCTION_PATH: 'web/',
    min: {
      dist: {
        src: ['src/js/script.js'],
        dest: 'src/js/script.min.js'
      }
    },
    cssmin: {
      dist: {
        src: ['src/css/style.css'],
        dest: 'src/css/script.min.css'
      }
    },
    sass: {
      dist: {
        options: {
          style: 'expanded'
        },
        files: {
          'build/css/style.css': 'src/css/style.scss',
          'build/css/responsive.css': 'src/css/responsive.scss'
        }
      }
    },
    htmlbuild: {
        dist: {
            src: 'index.html',
            dest: 'build/',
            options: {
                scripts: {
                    libs: [
                        'src/js/fontawesome.js',
                        'src/js/html5shiv.js',
                    ],
					main: 'src/js/script.js'
                },
                styles: {
                    libs: [
                        'src/css/fontawesome.min.css'
                    ],
					app: 'src/css/style.css'
                },
                data: {
                    version: "1.0.0",
                    title: "Teste Front-End Build"
                }
            }
        }
    }
  };

  grunt.initConfig(gruntConfig);

  grunt.loadNpmTasks('grunt-contrib-sass');
  grunt.loadNpmTasks('grunt-html-build');

  grunt.registerTask('default', ['sass', 'htmlbuild']);
};